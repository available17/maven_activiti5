package com.activiti5.sequenceFlow;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

public class SequenceFlowTest {
	 //获取流程引擎对象  
    //getDefaultProcessEngine方法内部会自动读取名为activiti.cfg.xml文件的配置信息  
    ProcessEngine processEngine=ProcessEngines.getDefaultProcessEngine();  

      
    /**部署流程定义*/  
    @Test  
    public void deploymentProcessDefinition_inputStream(){  
        //获得上传文件的输入流  
    	ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    	InputStream inputStreamBpmn = classloader.getResourceAsStream("diagrams/sequenceFlow.bpmn");
    	InputStream inputStreamPng = classloader.getResourceAsStream("diagrams/sequenceFlow.png");
//        InputStream inputStreamBpmn=getClass().getResourceAsStream("sequenceFlow.bpmn");  
//        InputStream inputStreamPng=getClass().getResourceAsStream("sequenceFlow.png");  
        //获取仓库服务，从类路径下完成部署  
        RepositoryService repositoryService=processEngine.getRepositoryService();  
        DeploymentBuilder deploymentBuilder=repositoryService.createDeployment();//创建一个部署对象  
        deploymentBuilder.name("连线");//添加部署的名称  
        deploymentBuilder.addInputStream("sequenceFlow.bpmn", inputStreamBpmn);  
        deploymentBuilder.addInputStream("sequenceFlow.png", inputStreamPng);  
//        deploymentBuilder.addClasspathResource("diagrams/MyProcess.bpmn");//从classpath的资源加载，一次只能加载一个文件  
//        deploymentBuilder.addClasspathResource("diagrams/MyProcess.png");//从classpath的资源加载，一次只能加载一个文件  
         
        Deployment deployment=deploymentBuilder.deploy();//完成部署  
          
        //打印我们的流程信息  
        System.out.println("部署Id:"+deployment.getId());  
        System.out.println("部署名称Name:"+deployment.getName());  
    }  
      
    /**启动流程引擎*/  
    @Test  
    public void startProcessInstance(){  
        //获取流程启动Service  
        RuntimeService runtimeService=processEngine.getRuntimeService();  
        //使用流程定义的key，key对应bpmn文件对应的id，  
        //(也是act_re_procdef表中对应的KEY_字段),默认是按照最新版本启动  
        String processDefinitionkey="sequenceFlow";//流程定义的key就是sequenceFlow  
        //获取流程实例对象  
        ProcessInstance processInstance=runtimeService.startProcessInstanceByKey(processDefinitionkey);  
        System.out.println("流程实例ID："+processInstance.getId());//流程实例ID  
        System.out.println("流程定义ID："+processInstance.getProcessDefinitionId());//流程定义ID  
    }  
      
    /**查询当前的个人任务(实际就是查询act_ru_task表)*/  
    @Test  
    public void findMyPersonalTask1(){  
        String assignee="赵六";  
        //获取事务Service  
        TaskService taskService=processEngine.getTaskService();  
        List<Task> taskList=taskService.createTaskQuery()//创建任务查询对象  
                   .taskAssignee(assignee)//指定个人任务查询，指定办理人  
                   .list();//获取该办理人下的事务列表  
          
        if(taskList!=null&&taskList.size()>0){  
            for(Task task:taskList){  
                System.out.println("任务ID："+task.getId());  
                System.out.println("任务名称："+task.getName());  
                System.out.println("任务的创建时间："+task.getCreateTime());  
                System.out.println("任务办理人："+task.getAssignee());  
                System.out.println("流程实例ID："+task.getProcessInstanceId());  
                System.out.println("执行对象ID："+task.getExecutionId());  
                System.out.println("流程定义ID："+task.getProcessDefinitionId());  
                System.out.println("#############################################");  
            }  
        }else{
        	System.out.println("暂无待办任务");
        }   
    }  
      
    /**完成我的任务*/  
    @Test  
    public void completeMyPersonalTask1(){  
        String taskId="50004";//上一次我们查询的任务ID  
        //完成任务的同时，设置流程变量，使用流程变量用来制定完成任务后，下一个连线，  
        //对应sequenceFlow.bpmn文件中${message=='不重要'}  
        Map<String,Object> variables=new HashMap<String,Object>();  
        variables.put("message", "重要");  
        TaskService taskService=processEngine.getTaskService();  
        taskService.complete(taskId,variables);//完成taskId对应的任务，并附带流程变量  
        System.out.println("完成ID为"+taskId+"的任务");  
    } 
    /**查询当前的个人任务(实际就是查询act_ru_task表)*/  
    @Test  
    public void findMyPersonalTask2(){  
        String assignee="田七";  
        //获取事务Service  
        TaskService taskService=processEngine.getTaskService();  
        List<Task> taskList=taskService.createTaskQuery()//创建任务查询对象  
                   .taskAssignee(assignee)//指定个人任务查询，指定办理人  
                   .list();//获取该办理人下的事务列表  
          
        if(taskList!=null&&taskList.size()>0){  
            for(Task task:taskList){  
                System.out.println("任务ID："+task.getId());  
                System.out.println("任务名称："+task.getName());  
                System.out.println("任务的创建时间："+task.getCreateTime());  
                System.out.println("任务办理人："+task.getAssignee());  
                System.out.println("流程实例ID："+task.getProcessInstanceId());  
                System.out.println("执行对象ID："+task.getExecutionId());  
                System.out.println("流程定义ID："+task.getProcessDefinitionId());  
                System.out.println("#############################################");  
            }  
        }else{
        	System.out.println("暂无待办任务");
        }   
    }  
      
    /**完成我的任务*/  
    @Test  
    public void completeMyPersonalTask2(){  
        String taskId="52503";//上一次我们查询的任务ID  
        //完成任务的同时，设置流程变量，使用流程变量用来制定完成任务后，下一个连线，  
        //对应sequenceFlow.bpmn文件中${message=='不重要'}  
        Map<String,Object> variables=new HashMap<String,Object>();  
        variables.put("message", "重要");  
        TaskService taskService=processEngine.getTaskService();  
        taskService.complete(taskId,variables);//完成taskId对应的任务，并附带流程变量  
        System.out.println("完成ID为"+taskId+"的任务");  
    } 
}
