package com.ssm.modular.login.dto;

import java.sql.Timestamp;

/**
 * @author ym
 * @date 2017年12月20日 上午9:29:22
 * @Version 1.0
 * <p>Description: 登录日志</p>
 */
public class SysLoginLog {

	/**
	* @Fields id : 主键id
	*/ 
	private String id;
	/**
	* @Fields mode : 登录/登出
	*/ 
	private String mode;
	/**
	* @Fields name : TODO
	*/ 
	/**
	* @Fields name : 登录/登出名称
	*/ 
	private String name;
	/**
	* @Fields time : 登录/登出时间
	*/ 
	private Timestamp time;
	/**
	* @Fields ip : 登录/登出IP
	*/ 
	private String ip;
	/**
	* @Fields state : 状态
	*/ 
	private String state;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getTime() {
		return time;
	}
	public void setTime(Timestamp timestamp) {
		this.time = timestamp;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
}
