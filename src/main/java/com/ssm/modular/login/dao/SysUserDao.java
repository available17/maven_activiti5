package com.ssm.modular.login.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import com.ssm.modular.login.dto.SysLoginLog;
import com.ssm.modular.sysUser.dto.SysUser;
import com.ssm.util.LocalhostIp;
import com.ssm.util.MybatisUtil;
import com.ssm.util.TimeStamp;
import com.ssm.util.UUid;
public class SysUserDao {
	  private SqlSessionFactory sessionFactory = MybatisUtil.getInstance();
	  private static final String LOGIN = "登录";
		public SysUser find(String username, String password) {
			SqlSession session = sessionFactory.openSession();
			try{
				SysUserMapper userMapper = session.getMapper(SysUserMapper.class);
				SysUser user= new SysUser();
				user.setUserName(username);
				user.setPassWord(password);
				SysUser   sysUser = userMapper.find(user);
				session.commit();
				return sysUser;
			}finally{
				session.close();
			}
	    }
		public void insertLoginLog(SysUser user, String state) {
			SqlSession session = sessionFactory.openSession();
			try{
				SysUserMapper userMapper = session.getMapper(SysUserMapper.class);
				LocalhostIp localhostIp = new LocalhostIp();
				SysLoginLog sysLoginLog = new SysLoginLog();
				sysLoginLog.setId(UUid.return_uuid());
				sysLoginLog.setMode(LOGIN);
				sysLoginLog.setName(user.getLoginName());
				sysLoginLog.setTime(TimeStamp.return_now());
				sysLoginLog.setIp(localhostIp.getLocalhostIp());
				sysLoginLog.setState(state);
				userMapper.insertLog(sysLoginLog);
				session.commit();
			}finally{
				session.close();
			}
		}
}
