package com.ssm.modular.login.dao;

import com.ssm.modular.login.dto.SysLoginLog;
import com.ssm.modular.sysUser.dto.SysUser;

public interface SysUserMapper {
    SysUser find(SysUser user);

	int insertLog(SysLoginLog sysLoginLog);
}
