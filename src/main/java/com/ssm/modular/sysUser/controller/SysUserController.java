package com.ssm.modular.sysUser.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ssm.modular.sysUser.dto.SysUser;
import com.ssm.modular.sysUser.service.ISysUserService;
import com.ssm.util.TimeStamp;
import com.ssm.util.UUid;

/**
 * @author ym
 * @date 2017年12月18日 上午9:43:30
 * @Version 1.0
 * <p>Description:系统用户</p>
 */

@Controller    
@RequestMapping("/sysUser")  
public class SysUserController {
		@Resource    
	   private ISysUserService sysUserService; 

		 /**
		 * @param request
		 * @param model
		 * @return
		 */
		@RequestMapping("/addSysUser")    
		   public String addUser(HttpServletRequest request,Model model){
			SysUser sysUser = new SysUser();
			sysUser.setId(UUid.return_uuid());
			sysUser.setLoginName(request.getParameter("loginName"));
			sysUser.setUserName(request.getParameter("userName"));
			sysUser.setPassWord(request.getParameter("passWord"));
			sysUser.setCreateTime(TimeStamp.return_now());
		    sysUserService.addSysUser(sysUser);    
		    return "redirect:/sysUser/sysUserList";    
		   } 
			 /**
			 * @return	
			 */
		@RequestMapping("/insertSysUserPage")    
		 public String insertSysUserPage(){    
		   return "sysUser/insertSysUser";    
		  }
		
		@RequestMapping("/sysUserList")
		public String sysUserList(Model model){
			  List<SysUser> sysUserList = sysUserService.getAllSysUser();    
		       model.addAttribute("sysUserList", sysUserList);    
			return "sysUser/sysUserList";
			
		}
		@RequestMapping("/deleteSysUser")
		public String deleteSysUser(HttpServletRequest request){
			sysUserService.deleteSysUser(request.getParameter("id"));
			  return "redirect:/sysUser/sysUserList";  
		}
		
		/**
		 * 菜单
		 * @return
		 */
		@RequestMapping("/sysUserMenu")    
		public String sysUserMenu(){    
		       return "sysUser/sysUserMenu";    
	  } 
}
