package com.ssm.modular.sysUser.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ssm.modular.login.dto.SysLoginLog;
import com.ssm.modular.sysUser.service.ISysLoginLogService;

/**
 * @author ym
 * @date 2017年12月20日 下午4:57:46
 * @Version 1.0
 * <p>Description: 登录日志</p>
 */
@Controller    
@RequestMapping("/loginLog")  
public class SysLoginLogController {
	@Resource  
	private ISysLoginLogService loginLogService;
	 @RequestMapping("/logList")    
	   public String logList(HttpServletRequest request,Model model){    
	       List<SysLoginLog> logList = loginLogService.queryAll();    
	       model.addAttribute("logList", logList);    
	       return "loginLog/sys-log";    
	   } 
}
