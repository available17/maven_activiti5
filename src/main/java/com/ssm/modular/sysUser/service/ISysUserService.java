package com.ssm.modular.sysUser.service;

import java.util.List;

import com.ssm.modular.sysUser.dto.SysUser;

/**
 * @author ym
 * @date 2017年12月18日 上午9:37:59
 * @Version 1.0
 * <p>Description:系统用户</p>
 */
public interface ISysUserService {

	/**
	 * @param sysUser
	 */
	void addSysUser(SysUser sysUser);

	List<SysUser> getAllSysUser();

	/**
	 * @param id
	 */
	void deleteSysUser(String id);

}
