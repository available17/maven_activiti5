package com.ssm.modular.sysUser.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ssm.modular.login.dto.SysLoginLog;
import com.ssm.modular.sysUser.dao.IsysLoginLogDao;
import com.ssm.modular.sysUser.dao.IsysUserDao;
import com.ssm.modular.sysUser.service.ISysLoginLogService;

/**
 * @author ym
 * @date 2017年12月20日 下午4:58:56
 * @Version 1.0
 * <p>Description:</p>
 */
@Service("sysLoginLogService")  
public class SysLoginLogServiceImpl implements ISysLoginLogService{
	@Resource
	private IsysLoginLogDao logDao;

	@Override
	public List<SysLoginLog> queryAll() {
		return logDao.queryAll();
	}

}
