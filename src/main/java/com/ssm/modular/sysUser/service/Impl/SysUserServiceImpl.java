package com.ssm.modular.sysUser.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ssm.modular.sysUser.dao.IsysUserDao;
import com.ssm.modular.sysUser.dto.SysUser;
import com.ssm.modular.sysUser.service.ISysUserService;

/**
 * @author ym
 * @date 2017年12月18日 上午9:40:29
 * @Version 1.0
 * <p>Description:系统用户</p>
 */
@Service("sysUserService")  
public class SysUserServiceImpl implements ISysUserService{
	@Resource
	private IsysUserDao sysDao;

	@Override
	public void addSysUser(SysUser sysUser) {
		sysDao.insertSysUser(sysUser);
	}

	@Override
	public List<SysUser> getAllSysUser() {
		return sysDao.queryAllSysUser();
	}

	@Override
	public void deleteSysUser(String id) {
		sysDao.delete(id);
	}
	
}
