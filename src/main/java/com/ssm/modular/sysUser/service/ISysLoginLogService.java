package com.ssm.modular.sysUser.service;

import java.util.List;

import com.ssm.modular.login.dto.SysLoginLog;

/**
 * @author ym
 * @date 2017年12月20日 下午4:58:38
 * @Version 1.0
 * <p>Description:</p>
 */
public interface ISysLoginLogService {

	List<SysLoginLog> queryAll();

}
