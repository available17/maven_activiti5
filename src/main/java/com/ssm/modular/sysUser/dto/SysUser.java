package com.ssm.modular.sysUser.dto;

import java.sql.Timestamp;

/**
 * @author ym
 * @date 2017年12月18日 上午9:37:59
 * @Version 1.0
 * <p>Description:系统用户</p>
 */
public class SysUser {
		private String id;//主键id
		private String loginName;//登录名
		private String userName;//用户名
		private String passWord;//密码
		private Timestamp createTime;//创建时间
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getLoginName() {
			return loginName;
		}
		public void setLoginName(String loginName) {
			this.loginName = loginName;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getPassWord() {
			return passWord;
		}
		public void setPassWord(String passWord) {
			this.passWord = passWord;
		}
		public Timestamp getCreateTime() {
			return createTime;
		}
		public void setCreateTime(Timestamp createTime) {
			this.createTime = createTime;
		}
		
		
}
