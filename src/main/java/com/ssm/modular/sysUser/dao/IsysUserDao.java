package com.ssm.modular.sysUser.dao;

import java.util.List;

import com.ssm.modular.sysUser.dto.SysUser;

/**
 * @author ym
 * @date 2017年12月18日 上午9:43:06
 * @Version 1.0
 * <p>Description:系统用户</p>
 */
public interface IsysUserDao {


	/**
	 * @param sysUser
	 */
	public void insertSysUser(SysUser sysUser);

	/**
	 * 
	 */
	public List<SysUser> queryAllSysUser();

	/**
	 * @param id
	 */
	public void delete(String id);

}
