package com.ssm.modular.sysUser.dao;

import java.util.List;

import com.ssm.modular.login.dto.SysLoginLog;

/**
 * @author ym
 * @date 2017年12月20日 下午5:00:18
 * @Version 1.0
 * <p>Description:</p>
 */
public interface IsysLoginLogDao {

	List<SysLoginLog> queryAll();

}
