package com.ssm.modular.user.dto;

public class User {
		private String id;    
		private Integer age;    
	    private String name;    
	    private String password;    
	    
	    
	    public Integer getAge() {
			return age;
		}

		public void setAge(Integer age) {
			this.age = age;
		}

		public String getId() {    
	        return id;    
	    }    
	    
	    public void setId(String id) {    
	        this.id = id;    
	    }    
	    
	    public String getName() {    
	        return name;    
	    }    
	    
	    public void setName(String name) {    
	        this.name = name;    
	    }    
	    
	    public String getPassword() {    
	        return password;    
	    }    
	    
	    public void setPassword(String password) {    
	        this.password = password == null ? null : password.trim();    
	    }    
}
