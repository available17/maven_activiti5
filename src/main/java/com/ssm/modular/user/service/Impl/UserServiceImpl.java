package com.ssm.modular.user.service.Impl;

import java.util.List;    

import javax.annotation.Resource;    

import org.springframework.stereotype.Service;    

import com.ssm.modular.user.dao.IUserDao;
import com.ssm.modular.user.dto.User;
import com.ssm.modular.user.service.IUserService;


@Service("userService")    
public class UserServiceImpl implements IUserService {    
@Resource    
private IUserDao userDao;    
   
public User getUserById(int userId) {    
   return userDao.queryByPrimaryKey(userId);    
}    

public void insertUser(User user) {    
   userDao.insertUser(user);    
}    

public void addUser(User user) {    
   userDao.insertUser(user);    
}    

public List<User> getAllUser() {    
   return userDao.getAllUser();    
}

@Override
public void deleteUser(String userId) {
	 userDao.deleteByPrimaryKey(userId);   
	
}    
}
