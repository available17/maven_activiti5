package com.ssm.modular.user.service;
import java.util.List;    

import com.ssm.modular.user.dto.User;

   
public interface IUserService {    
       
   public User getUserById(int userId);    
   
   public void insertUser(User user);    
   
   public void addUser(User user);    
   
   public List<User> getAllUser();

   public void deleteUser(String userId);

}
