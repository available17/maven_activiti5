package com.ssm.modular.user.controller;
import java.util.List;    

import javax.annotation.Resource;    
import javax.servlet.http.HttpServletRequest;    

import org.springframework.stereotype.Controller;    
import org.springframework.ui.Model;    
import org.springframework.web.bind.annotation.RequestMapping;    

import com.ssm.modular.user.dto.User;
import com.ssm.modular.user.service.IUserService;
import com.ssm.util.UUid;
@Controller    
@RequestMapping("/user")    
public class UserController {    
   @Resource    
   private IUserService userService;    
   @RequestMapping("/userList")    
   public String userList(HttpServletRequest request,Model model){    
       List<User> uList = userService.getAllUser();    
       model.addAttribute("uList", uList);    
       return "user/userList";    
   }    
       
   @RequestMapping("/showUser")    
   public String showUser(HttpServletRequest request,Model model){    
       int userId = Integer.parseInt(request.getParameter("id"));    
       User user = userService.getUserById(userId);    
       model.addAttribute("user", user);    
       return "showUser";    
   }    
   @RequestMapping("/deleteUser")    
   public String deleteUser(HttpServletRequest request,Model model){    
	   userService.deleteUser(request.getParameter("id"));    
	   return "redirect:/user/userList";  
   }    
       
     
       
   @RequestMapping("/addUser")    
   public String addUser(HttpServletRequest request,Model model){    
       User user = new User();  
       user.setId(UUid.return_uuid());
       user.setName(String.valueOf(request.getParameter("name")));    
       user.setPassword(String.valueOf(request.getParameter("password")));    
       user.setAge(Integer.parseInt(String.valueOf(request.getParameter("age"))));    
       userService.addUser(user);    
       return "redirect:/user/userList";    
   } 
   @RequestMapping("/addUserUI")    
   public String addUserUI(){    
       return "user/addUser";    
   } 
   
	@RequestMapping("/userMenu")    
	   public String userMenu(){    
		   return "user/userMenu";    
	 } 
}
