package com.ssm.util;

import java.io.Serializable;

/**
 * @author ym
 * @date 2017年12月18日 下午4:19:34
 * @Version 1.0
 * <p>Description:返回实现类</p>
 */
public class BaseResult implements Serializable {
	 /** 
     * serialVersionUID 
     */  
    private static final long serialVersionUID = -6673395649748854442L;  
      
    /** 
     * 请求唯一ID 
     */  
    private String  requestId;  
    /** 
     * 成功标记 
     */  
    private boolean  success;  
    /** 
     * 操作结果代码 
     */  
    private String   code;  
      
    /** 
     * 操作消息 
     */  
    private String  message;  
      
  
    public String getRequestId() {  
        return requestId;  
    }  
      
  
    public void setRequestId(String requestId) {  
        this.requestId = requestId;  
    }  
      
  
    public boolean isSuccess() {  
        return success;  
    }  
      
  
    public void setSuccess(boolean success) {  
        this.success = success;  
    }  
      
  
    public String getCode() {  
        return code;  
    }  
      
  
    public void setCode(String code) {  
        this.code = code;  
    }  
      
  
    public String getMessage() {  
        return message;  
    }  
      
  
    public void setMessage(String message) {  
        this.message = message;  
    }  
      
}
