package com.ssm.util;

import java.sql.Timestamp;

/**
 * @author ym
 * @date 2017年12月18日 上午10:06:04
 * @Version 1.0
 * <p>Description:</p>
 */
public class TimeStamp {
	/**
	 * @return 当前时间
	 */
	public static Timestamp return_now(){
		return new Timestamp(System.currentTimeMillis());
	}
}
