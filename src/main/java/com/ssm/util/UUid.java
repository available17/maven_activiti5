package com.ssm.util;

import java.util.UUID;

public class UUid {

	/**
	 * @return 返回uuid作为主键
	 */
	public static String return_uuid(){
		 return UUID.randomUUID().toString().replace("-", "");
		 
	}
}
