<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html style="overflow-y: hidden;">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>欢迎登录</title>
  <link rel="stylesheet" href="<%=path%>/source/css/login.css">
  　<link rel="shortcut icon" href="<%=path%>/source/images/index.ico" type="image/x-icon" />
  <script type="text/javascript" src="<%=path%>/source/js/jquery.min.js"></script>
  <script type="text/javascript" src="<%=path%>/source/js/lay/modules/layer.js"></script>
  <script type="text/javascript" src="<%=path%>/source/lib/layui/layui.js" ></script>
	</head>
	<body class="login-bg" >
	    <div  id="large-header" class="large-header login container demo-1" style="overflow:hidden" >
    <canvas id="demo-canvas"></canvas>
        <form method="post" class="layui-form" action="loginServlet" id="form">
        <span class="u_user"></span>
            <input name="username" id="username" placeholder="请输入账户"  type="text" lay-verify="required" class="layui-input" style="color: #FFFFFF !important;padding-left: 50px" >
            <hr class="hr15">
            <span class="us_uer"></span>
            <input name="password" id="password" placeholder="请输入密码" lay-verify="required"   type="password" class="layui-input" style="color: #FFFFFF !important;padding-left: 50px" >
            <hr class="hr15">
            <input value="登录" lay-submit lay-filter="login" style="width:100%;background: #0096e6;" type="submit">
            <hr class="hr20" >
        </form>
    </div>
  <script type="text/javascript" src="<%=path%>/source/js/TweenLite.min.js"></script>
  <script type="text/javascript" src="<%=path%>/source/js/EasePack.min.js"></script>
  <script type="text/javascript" src="<%=path%>/source/js/rAF.js"></script>
  <script type="text/javascript" src="<%=path%>/source/js/demo-1.js"></script>
  <script type="text/javascript" src="<%=path%>/source/lib/layui/layui.js"></script>
  <script type="text/javascript" src="<%=path%>/source/js/x-layui.js"></script>
    <!-- 底部结束 -->
     <%
     Object message = request.getAttribute("message");
   	 if(message!=null && !"".equals(message)){
	 %>
	     <script type="text/javascript" >
	         alert('<%=message%>');
	     </script>
	 <%} %>
</body>
</html>