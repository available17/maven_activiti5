<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>  
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
    <html>  
    <head>  
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  
    <title>列表</title> 
    <script type="text/javascript">
	    function deleteUser(id){
	    	form.action="<%=path%>/user/deleteUser?id="+id;
	    	form.submit();
	    }
	    function indexs(){
	    	form.action="<%=path%>/index.jsp";
	    	form.submit();
	    }
	    function insert() {
	    	form.action="<%=path%>/user/addUserUI";
	    	form.submit();
	    }
    </script> 
    </head>  
    <body style="height: 100%">  
    <form name="form" method="post">
         <table>
        <c:forEach items="${uList}" var="item">
		  <tr>
		  	<td align="right" width="10%" nowrap>名称：</td>
			<td align="left"> ${item.name}</td>
		  </tr>
		  <tr>
		  	<td align="right" width="10%" nowrap>年龄：</td>
			<td align="left"> ${item.age}</td>
			<td><button type="button" onclick="deleteUser('${item.id}')" >删除</button></td>
		  </tr>
        </c:forEach>
       <button type="button" onclick="javascript:indexs()" >首页</button>
       <button type="button"  onclick="javascript:insert();"> 新增</button>
  </table>
  </form>
    </body>  
    </html>  