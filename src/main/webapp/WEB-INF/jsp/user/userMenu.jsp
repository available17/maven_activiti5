<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">   
    <title>人员管理</title>       
  <meta http-equiv="pragma" content="no-cache">
  <meta http-equiv="cache-control" content="no-cache">
  <meta http-equiv="expires" content="0">   
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="This is my page">
 </head>
 <script type="text/javascript">
 function insert() {
		form.action="<%=path%>/user/addUserUI";
		form.submit();
	}
	function queryList() {
		 form.action="<%=path%>/user/userList";
		 form.submit();
	}
	 function indexPage() {
		 form.action="<%=path%>/index.jsp";
		 form.submit();
	}
 </script>
 <body style="height: 100%">
 <form name="form" method="post">
 <button type="button"  onclick="javascript:indexPage();">首页</button>
 <button type="button"  onclick="javascript:insert();"> 人员新增</button>
<button type="button" onclick="javascript:queryList();">人员列表 </button>
 
 </form>
 </body>
</html>