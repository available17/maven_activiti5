<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">   
    <title>系统用户</title>       
  <meta http-equiv="pragma" content="no-cache">
  <meta http-equiv="cache-control" content="no-cache">
  <meta http-equiv="expires" content="0">   
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="This is my page">
 </head>
 <script type="text/javascript">
	 function inserSysUser() {
		 form.action="<%=path%>/sysUser/insertSysUserPage";
		 form.submit();
	}
	 function SysUserList() {
		 form.action="<%=path%>/sysUser/sysUserList";
		 form.submit();
	}
	 function indexPage() {
		 form.action="<%=path%>/index.jsp";
		 form.submit();
	}
 </script>
 <body style="height: 100%">
<form name="form" method="post">
 <button type="button" onclick="javascript:indexPage();">首页 </button>
 <button type="button" onclick="javascript:inserSysUser();">用户新增 </button>
 <button type="button" onclick="javascript:SysUserList();">用户列表 </button>
 </form>
 
 </body>
</html>