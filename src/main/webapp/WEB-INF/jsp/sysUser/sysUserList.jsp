<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>系统用户列表</title>
 <script type="text/javascript">
	    function deleteSysUser(id){
	    	form.action="<%=path%>/sysUser/deleteSysUser?id="+id;
	    	form.submit();
	    }
	    function indexs(){
	    	form.action="<%=path%>/index.jsp";
	    	form.submit();
	    }
	    function insert() {
	    	 form.action="<%=path%>/sysUser/insertSysUserPage";
	    	 form.submit();
	    }
    </script> 
</head>
<body style="height: 100%">
 <form name="form" method="post">
         <table>
        <c:forEach items="${sysUserList}" var="item">
		  <tr>
		  	<td align="right" width="10%" nowrap>用户名：</td>
			<td align="left"> ${item.userName}</td>
		  </tr>
		  <tr>
		  		<td align="right" width="10%" nowrap>登录名：</td>
			<td align="left"> ${item.loginName}</td>
		  </tr>
		  <tr>
		  	<td align="right" width="10%" nowrap>创建时间：</td>
			<td align="left"><fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
			<td><button type="button" onclick="deleteSysUser('${item.id}')" >删除</button></td>
		  </tr>
        </c:forEach>
       <button type="button" onclick="javascript:indexs()" >首页</button>
       <button type="button"  onclick="javascript:insert();"> 新增</button>
  </table>
  </form>
</body>
</html>