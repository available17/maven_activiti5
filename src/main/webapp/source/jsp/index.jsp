<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>    
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>欢迎登录</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="<%=path%>/source/images/index.ico" type="image/x-icon" />
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="<%=path%>/source/css/x-admin.css" media="all">
<link rel="stylesheet" href="<%=path%>/source/css/layui/layui.css"  media="all">
</head>
<body style="height: 100%">
<div class="layui-layout layui-layout-admin">
  <div class="layui-header header header-demo">
    <div class="layui-main">
    <div class="admin-logo-box">
				<a class="logo" href="#" title="logo">欢迎登录</a>
				<div class="larry-side-menu">
					<i class="fa fa-th-large" aria-hidden="false"></i>
				</div>
			</div>
            <ul class="layui-nav layui-layout-left layui-ygyd-menu" style="position:absolute; left:250px;">
    </ul>
    
      <ul class="layui-nav" lay-filter="" style="margin-right: 40px">
      <li class="layui-nav-item">
                        <a href="" title="消息">
                            <i class="layui-icon" style="top: 1px;">&#xe63a;</i>
                        </a>
                        </li>
        <li class="layui-nav-item"><img src="<%=path%>/source/images/logo.png" class="layui-circle" style="border: 2px solid #A9B7B7;" width="35px" alt=""></li>
        <li class="layui-nav-item"> <a href="javascript:;">admin</a>
          <dl class="layui-nav-child">
            <!-- 二级菜单 -->
            <dd><a href="">个人信息</a></dd>
            <dd><a href="<%=path%>/login.jsp">切换帐号</a></dd>
            <dd><a href="<%=path%>/login.jsp">退出</a></dd>
          </dl>
        </li>
      </ul>
    </div>
  </div>
  <div class="layui-side layui-bg-black x-side" style="left:-200px;">
    <div class="layui-side-scroll">
      <ul class="layui-nav layui-nav-tree site-demo-nav" lay-filter="side">
        <li class="layui-nav-item"> <a class="javascript:;" href="javascript:;"> <i class="layui-icon" style="top: 3px;">&#xe62d;</i><cite>机构管理</cite> </a>
          <dl class="layui-nav-child">
            <dd class="">
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/question-list.jsp"> <cite>机构列表</cite> </a> </dd>
            </dd>
          </dl>
        </li>
        <li class="layui-nav-item"> <a class="javascript:;" href="javascript:;"> <i class="layui-icon" style="top: 3px;">&#xe642;</i><cite>订单管理</cite> </a>
          <dl class="layui-nav-child">
            <dd class="">
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/welcome.jsp"> <cite>订单列表（待开发）</cite> </a> </dd>
            </dd>
          </dl>
        </li>
        <li class="layui-nav-item"> <a class="javascript:;" href="javascript:;"> <i class="layui-icon" style="top: 3px;">&#xe613;</i><cite>管理员管理</cite> </a>
          <dl class="layui-nav-child">
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/admin-list.jsp"> <cite>管理员列表</cite> </a> </dd>
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/admin-role.jsp"> <cite>角色管理</cite> </a> </dd>
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/admin-cate.jsp"> <cite>权限分类</cite> </a> </dd>
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/admin-rule.jsp"> <cite>权限管理</cite> </a> </dd>
          </dl>
        </li>
        <li class="layui-nav-item"> <a class="javascript:;" href="javascript:;"> <i class="layui-icon" style="top: 3px;">&#xe629;</i><cite>系统统计</cite> </a>
          <dl class="layui-nav-child">
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/echart.jsp"> <cite>统计报表</cite> </a> </dd>
          </dl>
        </li>
        <li class="layui-nav-item"> <a class="javascript:;" href="javascript:;"> <i class="layui-icon" style="top: 3px;">&#xe614;</i><cite>系统设置</cite> </a>
          <dl class="layui-nav-child">
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/sys-set.jsp"> <cite>系统设置</cite> </a> </dd>
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/sys-data.jsp"> <cite>数字字典</cite> </a> </dd>
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/jsp/sys-shield.jsp"> <cite>屏蔽词</cite> </a> </dd>
            <dd class=""> <a href="javascript:void(0);" _href="<%=path%>/loginLog/logList" > <cite>系统日志</cite> </a> </dd>
          </dl>
        </li>
          <li class="layui-nav-item"> <a class="javascript:;" href="javascript:;"> <i class="layui-icon" style="top: 3px;">&#xe629;</i><cite>Web前端</cite> </a>
          <dl class="layui-nav-child">
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/WebMould/perfect/index.html"> <cite>登录模板</cite> </a> </dd>
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/WebMould/moban2013/index.html"> <cite>模板2013</cite> </a> </dd>
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/WebMould/moban216/index.html"> <cite>BUI前端框架</cite> </a> </dd>
            <dd class=""> <a href="javascript:;" _href="<%=path%>/source/WebMould/X-admin/index.html"> <cite>X-admin2.0</cite> </a> </dd>
          </dl>
        </li>
        <li class="layui-nav-item" style="height: 30px; text-align: center"> </li>
      </ul>
    </div>
  </div>
  <div class="layui-tab layui-tab-card site-demo-title x-main" lay-filter="x-tab" lay-allowclose="true" style="left: 0px;border-left: solid 2px #F2F2F2;">
    <ul class="layui-tab-title">
      <li class="layui-this"> 我的桌面 <i class="layui-icon layui-unselect layui-tab-close">ဆ</i> </li>
    </ul>
    <div class="layui-tab-content site-demo site-demo-body">
      <div class="layui-tab-item layui-show">
        <iframe frameborder="0" src="<%=path%>/source/jsp/welcome.jsp" class="x-iframe"></iframe>
      </div>
    </div>
  </div>
  <div class="site-mobile-shade"> </div>
</div>
<script src="<%=path%>/source/lib/layui/layui.js" charset="utf-8"></script> 
<script src="<%=path%>/source/js/x-admin.js"></script> 
</body>
</html>